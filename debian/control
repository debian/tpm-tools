Source: tpm-tools
Section: admin
Priority: optional
Maintainer: Pierre Chifflier <pollux@debian.org>
Build-Depends:
 debhelper (>= 10), automake, libopencryptoki-dev,
 libtspi-dev (>= 0.3.10),
 quilt, autopoint
Standards-Version: 4.6.1
Homepage: http://trousers.sourceforge.net/
Rules-Requires-Root: no
Vcs-Browser: https://salsa.debian.org/debian/tpm-tools
Vcs-Git: https://salsa.debian.org/debian/tpm-tools.git

Package: tpm-tools
Architecture: any
Depends: ${misc:Depends}, ${shlibs:Depends}, trousers
Description: Management tools for the TPM hardware (tools)
 tpm-tools is a group of tools to manage and utilize the Trusted Computing
 Group's TPM hardware. TPM hardware can create, store and use RSA keys securely
 (without ever being exposed in memory), verify a platform's software state
 using cryptographic hashes and more.
 .
 This package contains tools to allow the platform administrator the ability
 to manage and diagnose the platform's TPM.

Package: tpm-tools-pkcs11
Architecture: any
Depends: ${misc:Depends}, ${shlibs:Depends}, opencryptoki, trousers
Breaks: tpm-tools (<< 1.3.9.1-0.2~)
Replaces: tpm-tools (<< 1.3.9.1-0.2~)
Description: Management tools for the TPM hardware (PKCS#11 tools)
 tpm-tools is a group of tools to manage and utilize the Trusted Computing
 Group's TPM hardware. TPM hardware can create, store and use RSA keys securely
 (without ever being exposed in memory), verify a platform's software state
 using cryptographic hashes and more.
 .
 This package contains commands to utilize some of the capabilities available
 in the TPM PKCS#11 interface implemented in the openCryptoki project.

Package: libtpm-unseal1
Section: libs
Architecture: any
Pre-Depends: ${misc:Pre-Depends}
Depends: ${misc:Depends}, ${shlibs:Depends}
Description: Management tools for the TPM hardware (library)
 tpm-tools is a group of tools to manage and utilize the Trusted Computing
 Group's TPM hardware. TPM hardware can create, store and use RSA keys
 securely (without ever being exposed in memory), verify a platform's
 software state using cryptographic hashes and more.
 .
 This package contains the library.

Package: libtpm-unseal-dev
Section: libdevel
Architecture: any
Depends: ${misc:Depends}, libtpm-unseal1 (= ${binary:Version})
Description: Management tools for the TPM hardware (development)
 tpm-tools is a group of tools to manage and utilize the Trusted Computing
 Group's TPM hardware. TPM hardware can create, store and use RSA keys
 securely (without ever being exposed in memory), verify a platform's
 software state using cryptographic hashes and more.
 .
 This package contains the development files.
